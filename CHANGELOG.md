### [1.0.0] - 2022-05-17

- Version updated to stable, ready to go

### [0.3.1] - 2022-05-17

### Added

- README.md file

### [0.3.0] - 2022-05-16

### Added

- FinishFootballGame functionality has been developed and tested successfully
- GetPreviousGamesSummary functionality has been developed and tested successfully
- All project's expected functionalities are done and ready to use

### [0.2.0] - 2022-05-15

### Added

- UpdateFootballGame functionality has been developed and tested successfully. It allows to update the current football
  game score on the scoreboard.

### [0.1.0] - 2022-05-15

### Added

- FootballGame class as an implemented representation of a football game
- [WIP] WorldCup2018FootballScoreboard class implementation of a football scoreboard to handle WorldCup2018 football
  games
- WorldCup2018Participant enumerated class includes all 32 FIFA World Cup 2018 participants
- Hierarchy structure for this library
- JUnit tests to cover existing functionality
- Javadoc descriptions for existing functionality

### [0.0.4] - 2022-05-13

### Added

- build with javadoc.jar task created, so complete java documentation will be auto-generated when a build task is
  executed

### Updated

- junit jupiter dependencies updated

### [0.0.3] - 2022-05-13

### Added

- GitLab pipeline integrated, build check, unit test report generated during the test stage
- .gitlab-ci.yml file created

### [0.0.2] - 2022-05-13

### Added

Gradle config:

- improvement of the 'jar' task - for building auto-generated manifest.mf file with impl details
- showProperties task created that shows the project version
- gradle.properties file added
- maven-publish plugin and publishing task created for publishing this library

### [0.0.1] - 2022-05-12

### Added

- Project initiation
- Git integration
- Gradle structure
- ScoreBoard initiation
