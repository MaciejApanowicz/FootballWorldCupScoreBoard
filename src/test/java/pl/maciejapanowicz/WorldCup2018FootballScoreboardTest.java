package pl.maciejapanowicz;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class WorldCup2018FootballScoreboardTest {

    @Test
    public void worldCupFootballScoreboardIsScoreboardInstance() {
        assertInstanceOf(Scoreboard.class, new WorldCup2018FootballScoreboard());
    }

    @Test
    public void createNewWorldCup2018FootballScoreBoardWithoutAnyGameOnIt() {
        assertNull(new WorldCup2018FootballScoreboard().getCurrentFootballGame());
    }

    @Test
    public void addFootballGameOnWC2018FootballScoreboard() {
        WorldCup2018FootballScoreboard wc2018footballScoreboard = new WorldCup2018FootballScoreboard();
        wc2018footballScoreboard.startGame(WorldCup2018Participant.SPAIN, WorldCup2018Participant.BELGIUM);
        assertNotNull(wc2018footballScoreboard.getCurrentFootballGame());
        assertInstanceOf(FootballGame.class, wc2018footballScoreboard.getCurrentFootballGame());
    }

    @Test
    public void displayCurrentGameOnTheBoardWhenGameStarted() {
        WorldCup2018FootballScoreboard wc2018footballScoreboard = new WorldCup2018FootballScoreboard();
        wc2018footballScoreboard.startGame(WorldCup2018Participant.SPAIN, WorldCup2018Participant.BELGIUM);
        wc2018footballScoreboard.displayCurrentGame();
    }

    @Test
    public void displayCurrentGameOnTheBoardWhenNoGameWasAddedYet() {
        new WorldCup2018FootballScoreboard().displayCurrentGame();
    }

    @Test
    public void updateFootballGameThatStartedWithCorrectScore() throws Exception {
        WorldCup2018FootballScoreboard wc2018footballScoreboard = new WorldCup2018FootballScoreboard();
        wc2018footballScoreboard.startGame(WorldCup2018Participant.SPAIN, WorldCup2018Participant.SAUDI_ARABIA);
        wc2018footballScoreboard.updateFootballGame(3, 2);
    }

    @Test
    public void updateFootballGameThatStartedWithoutGoalsScored() throws Exception {
        WorldCup2018FootballScoreboard wc2018footballScoreboard = new WorldCup2018FootballScoreboard();
        wc2018footballScoreboard.startGame(WorldCup2018Participant.SPAIN, WorldCup2018Participant.SAUDI_ARABIA);
        wc2018footballScoreboard.updateFootballGame(0, 0);
        assertEquals(0, wc2018footballScoreboard.getCurrentFootballGame().getHomeTeamScore());
        assertEquals(0, wc2018footballScoreboard.getCurrentFootballGame().getAwayTeamScore());
    }

    @Test
    public void updateFootballGameWithNegativeScoreNotAllowed() {
        assertThrows(IllegalArgumentException.class, () -> {
            WorldCup2018FootballScoreboard wc2018footballScoreboard = new WorldCup2018FootballScoreboard();
            wc2018footballScoreboard.startGame(WorldCup2018Participant.SPAIN, WorldCup2018Participant.SAUDI_ARABIA);
            wc2018footballScoreboard.updateFootballGame(-2, -1);
            assertTrue(wc2018footballScoreboard.getCurrentFootballGame().getHomeTeamScore() >= 0 &&
                    wc2018footballScoreboard.getCurrentFootballGame().getAwayTeamScore() >= 0);
        });
    }

    @Test
    public void updateFootballGameWhenNoCurrentGameIsOnBoard() {
        assertThrows(Exception.class, () -> {
            WorldCup2018FootballScoreboard wc2018footballScoreboard = new WorldCup2018FootballScoreboard();
            wc2018footballScoreboard.updateFootballGame(3, 2);
            assertEquals(3, wc2018footballScoreboard.getCurrentFootballGame().getHomeTeamScore());
            assertEquals(2, wc2018footballScoreboard.getCurrentFootballGame().getAwayTeamScore());
        });
    }

    @Test
    public void finishGameAndCleanBoardWhenNoGameStarted() {
        WorldCup2018FootballScoreboard worldCup2018FootballScoreboard = new WorldCup2018FootballScoreboard();
        worldCup2018FootballScoreboard.finishFootballGame();
        assertNull(worldCup2018FootballScoreboard.getCurrentFootballGame());
    }


    @Test
    public void finishGameAndCleanBoardWhenGameStarted() {
        WorldCup2018FootballScoreboard worldCup2018FootballScoreboard = new WorldCup2018FootballScoreboard();
        worldCup2018FootballScoreboard.startGame(WorldCup2018Participant.MEXICO, WorldCup2018Participant.COSTA_RICA);
        worldCup2018FootballScoreboard.finishFootballGame();
        assertNull(worldCup2018FootballScoreboard.getCurrentFootballGame());
    }

    @Test
    public void finishGameAndCleanBoardWhenGameUpdated() throws Exception {
        WorldCup2018FootballScoreboard worldCup2018FootballScoreboard = new WorldCup2018FootballScoreboard();
        worldCup2018FootballScoreboard.startGame(WorldCup2018Participant.MEXICO, WorldCup2018Participant.COSTA_RICA);
        worldCup2018FootballScoreboard.updateFootballGame(0, 5);
        worldCup2018FootballScoreboard.finishFootballGame();
        assertNull(worldCup2018FootballScoreboard.getCurrentFootballGame());
    }

    @Test
    public void gamesAreCollectedAfterTheyAreFinished() throws Exception {
        WorldCup2018FootballScoreboard scoreboard = new WorldCup2018FootballScoreboard();
        scoreboard.startGame(WorldCup2018Participant.MEXICO, WorldCup2018Participant.COSTA_RICA);
        scoreboard.finishFootballGame();

        scoreboard.startGame(WorldCup2018Participant.SPAIN, WorldCup2018Participant.BRAZIL);
        scoreboard.updateFootballGame(10, 2);
        scoreboard.finishFootballGame();

        scoreboard.startGame(WorldCup2018Participant.GERMANY, WorldCup2018Participant.FRANCE);
        scoreboard.finishFootballGame();

        assertEquals(3, scoreboard.getPreviousFootballGame().size());
    }

    @Test
    public void getPreviousGamesSummaryFunctionalCase() throws Exception {
        WorldCup2018FootballScoreboard testScoreboard = new WorldCup2018FootballScoreboard();
        testScoreboard.startGame(WorldCup2018Participant.MEXICO, WorldCup2018Participant.CROATIA);
        testScoreboard.updateFootballGame(0, 5);
        testScoreboard.finishFootballGame();

        testScoreboard.startGame(WorldCup2018Participant.SPAIN, WorldCup2018Participant.BRAZIL);
        testScoreboard.updateFootballGame(10, 2);
        testScoreboard.finishFootballGame();

        testScoreboard.startGame(WorldCup2018Participant.GERMANY, WorldCup2018Participant.FRANCE);
        testScoreboard.updateFootballGame(2, 2);
        testScoreboard.finishFootballGame();

        testScoreboard.startGame(WorldCup2018Participant.URUGUAY, WorldCup2018Participant.IRAN);
        testScoreboard.updateFootballGame(6, 6);
        testScoreboard.finishFootballGame();

        testScoreboard.startGame(WorldCup2018Participant.ARGENTINA, WorldCup2018Participant.AUSTRALIA);
        testScoreboard.updateFootballGame(3, 1);
        testScoreboard.finishFootballGame();

        List<FootballGame> previousGameSummaryOrder = testScoreboard.getPreviousGamesSummary();
        assertEquals("Uruguay", previousGameSummaryOrder.get(0).getHomeTeam().getParticipantName());
        assertEquals("Spain", previousGameSummaryOrder.get(1).getHomeTeam().getParticipantName());
        assertEquals("Mexico", previousGameSummaryOrder.get(2).getHomeTeam().getParticipantName());
        assertEquals("Argentina", previousGameSummaryOrder.get(3).getHomeTeam().getParticipantName());
        assertEquals("Germany", previousGameSummaryOrder.get(4).getHomeTeam().getParticipantName());
    }

    @Test
    public void correctNumberOfGamesReturnedInSummary() throws Exception {
        WorldCup2018FootballScoreboard testScoreboard = new WorldCup2018FootballScoreboard();

        testScoreboard.startGame(WorldCup2018Participant.SPAIN, WorldCup2018Participant.BRAZIL);
        testScoreboard.updateFootballGame(10, 2);
        testScoreboard.finishFootballGame();

        testScoreboard.startGame(WorldCup2018Participant.URUGUAY, WorldCup2018Participant.IRAN);
        testScoreboard.updateFootballGame(6, 6);
        testScoreboard.finishFootballGame();

        testScoreboard.startGame(WorldCup2018Participant.ARGENTINA, WorldCup2018Participant.AUSTRALIA);
        testScoreboard.updateFootballGame(3, 1);
        testScoreboard.finishFootballGame();

        assertEquals(3, testScoreboard.getPreviousGamesSummary().size());
    }

    @Test
    public void previousGamesSummaryWhenNoGameExists() {
        WorldCup2018FootballScoreboard testScoreboard = new WorldCup2018FootballScoreboard();
        testScoreboard.getPreviousGamesSummary();
    }
}
