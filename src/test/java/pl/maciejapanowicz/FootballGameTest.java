package pl.maciejapanowicz;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FootballGameTest {

    @Test
    public void eachFootballGameHasExactlyTwoParticipants() {
        FootballGame footballGame = new FootballGame(WorldCup2018Participant.BELGIUM, WorldCup2018Participant.COLOMBIA);
        assertEquals(2, footballGame.numberOfParticipants());
        assertTrue(footballGame.hasParticipants());
    }

    @Test
    public void teamScoreMustNotBeNegative() {
        FootballGame footballGame = new FootballGame(WorldCup2018Participant.BRAZIL, WorldCup2018Participant.AUSTRALIA);
        assertTrue(footballGame.getHomeTeamScore() >= 0 && footballGame.getAwayTeamScore() >= 0);
    }

    @Test
    public void gamesAreEqualWhenTeamsAndScoreAndDateAreEqual() {
        FootballGame game = new FootballGame(WorldCup2018Participant.ARGENTINA, WorldCup2018Participant.BELGIUM);
        FootballGame sameGame = new FootballGame(WorldCup2018Participant.ARGENTINA, WorldCup2018Participant.BELGIUM);
        assertEquals(game, sameGame);
    }

    @Test
    public void gamesAreNotEqualWhenDifferentTeamsPlay() {
        FootballGame footballGame = new FootballGame(WorldCup2018Participant.POLAND, WorldCup2018Participant.COLOMBIA);
        FootballGame footballGame1 = new FootballGame(WorldCup2018Participant.POLAND, WorldCup2018Participant.SENEGAL);
        assertNotEquals(footballGame, footballGame1);
    }

    @Test
    public void gamesAreNotEqualWhenGameDateDiffer() {
        FootballGame footballGame = new FootballGame(WorldCup2018Participant.CROATIA, WorldCup2018Participant.EGYPT, 2018, 6, 6);
        FootballGame footballGame1 = new FootballGame(WorldCup2018Participant.CROATIA, WorldCup2018Participant.EGYPT, 2018, 6, 28);
        assertNotEquals(footballGame, footballGame1);
    }

    @Test
    public void teamMustNotPlayWithItselfAsOpponent() {
        assertThrows(IllegalArgumentException.class, () -> {
            FootballGame footballGame = new FootballGame(WorldCup2018Participant.POLAND, WorldCup2018Participant.POLAND);
        });
    }
}
