package pl.maciejapanowicz;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WorldCup2018ParticipantTest {

    @Test
    public void simpleWorldCup2018ParticipantEnumValueOfTest() {
        WorldCup2018Participant worldCup2018Participant = WorldCup2018Participant.POLAND;
        assertEquals(WorldCup2018Participant.valueOf("POLAND"), worldCup2018Participant);
    }

    @Test
    public void enumNameAndParticipantNameNotEqual() {
        WorldCup2018Participant worldCup2018Participant = WorldCup2018Participant.ARGENTINA;
        assertNotEquals(worldCup2018Participant.name(), worldCup2018Participant.getParticipantName());
    }

    @Test
    public void eachParticipantHasParticipantName() {
        WorldCup2018Participant worldCup2018Participant = WorldCup2018Participant.SENEGAL;
        assertNotNull(worldCup2018Participant.getParticipantName());
        assertNotSame(0, worldCup2018Participant.getParticipantName().length());
    }
}