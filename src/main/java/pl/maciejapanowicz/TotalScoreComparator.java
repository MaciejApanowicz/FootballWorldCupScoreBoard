package pl.maciejapanowicz;

import java.util.*;

/**
 * Comparator created for sorting football games by total score in descending order.
 * Those games with the same total score will be sorted by the most recently added which are those with higher gameId value.
 *
 * @author Maciej Apanowicz
 */
public class TotalScoreComparator implements Comparator<FootballGame> {
    /**
     * Compares two football game objects by total score. If total score of both equal then compares gameId attributes of both objects to find out which game is most recent.
     *
     * @param o1 this football game
     * @param o2 football game to be compared
     * @return int value less than 0 if this football game is intended to be classified as lower than football game to be compared in order specified by the rules of TotalScoreComparator, value greater than 0 otherwise.
     */
    @Override
    public int compare(FootballGame o1, FootballGame o2) {
        int result = (o1.getTotalScore().compareTo(o2.getTotalScore()) * (-1));
        if (result == 0) {
            return (o1.getGameId().compareTo(o2.getGameId()) * (-1));
        } else {
            return result;
        }
    }
}
