package pl.maciejapanowicz;

import java.util.ArrayList;
import java.util.List;

/**
 * Football World Cup 2018 Football Scoreboard Implementation
 * This is a board on which football games are displayed during FIFA World Cup 2018 tournament.
 * <p>
 * This clas implemented functionalities include:
 * - to start a new football game and add it on the board,
 * - to display the current footballGame home team and away team alongside with the score of both teams,
 * - to update the score in the current football game that is being played,
 * - to finish the current football game, then add it to the previous game set and to clean the board when it is done.
 * - get the summary of the previous football games that were displayed on this board.
 *
 * @author Maciej Apanowicz
 */
public class WorldCup2018FootballScoreboard implements FootballScoreboard {
    private FootballGame currentFootballGame;
    private ArrayList<FootballGame> previousFootballGame;

    /**
     * No argument constructor
     */
    public WorldCup2018FootballScoreboard() {
        previousFootballGame = new ArrayList<>();
    }

    /**
     * Starts a football game between two teams and add this game on the board as a current football game
     *
     * @param homeTeam a team that plays as a host
     * @param awayTeam a team that plays as a guest
     */
    public void startGame(GameParticipant homeTeam, GameParticipant awayTeam) {
        currentFootballGame = new FootballGame(homeTeam, awayTeam);
    }

    /**
     * Starts a football game between two teams and add this game on the board as a current football game
     *
     * @param homeTeam a team that plays as a host
     * @param awayTeam a team that plays as a guest
     * @param year     year when the game will take place
     * @param month    month represented by integer value when the game will take place
     * @param day      day when the game will take place
     */
    public void startGame(GameParticipant homeTeam, GameParticipant awayTeam, int year, int month, int day) {
        currentFootballGame = new FootballGame(homeTeam, awayTeam, year, month, day);
    }

    /**
     * Updates current football game on this board. Throws exception when this method is called when no game is on the board. To update a game firstly it is needed to start a game.
     *
     * @param homeTeamScore number of goals scored by the home team. Must not be negative because negative goal score never exist.
     * @param awayTeamScore number of goals scored by the away team. Must not be negative because negative goal score never exist.
     * @throws Exception thrown when this method is called when no game on the board exists
     */
    @Override
    public void updateFootballGame(Integer homeTeamScore, Integer awayTeamScore) throws Exception {
        if (currentFootballGame == null) {
            throw new Exception("No current game exists on the board!");
        } else {
            if (homeTeamScore >= 0 && awayTeamScore >= 0) {
                currentFootballGame.setHomeTeamScore(homeTeamScore);
                currentFootballGame.setAwayTeamScore(awayTeamScore);
            } else {
                throw new IllegalArgumentException("Score which is a number of goals scored by a team must not be negative");
            }
        }
    }

    /**
     * Finishes the current football game and cleans this football board. The finished game is added to the list with previous games
     */
    @Override
    public void finishFootballGame() {
        if (currentFootballGame != null) {
            currentFootballGame.setTotalScore();
            currentFootballGame.setGameId();
            previousFootballGame.add(currentFootballGame);
            currentFootballGame = null;
        }
    }

    /**
     * Returns the summary of previous games in the system (handled by this board) in a form of a list, that is sorted out by total score.
     * If total score of games is equal then these games will be returned in most recently added to the board.
     *
     * @return previous games summary
     */
    @Override
    public List<FootballGame> getPreviousGamesSummary() {
        TotalScoreComparator myComparator = new TotalScoreComparator();
        previousFootballGame.stream().sorted(myComparator).forEach(System.out::println);
        List<FootballGame> previousGameSummary = previousFootballGame.stream().sorted(myComparator).toList();
        return previousGameSummary;
    }

    /**
     * Displays current World Cup 2018 football game on the board
     */
    @Override
    public void displayCurrentGame() {
        if (currentFootballGame == null) {
            System.out.println("Home team - Away team  [0:0]");
        } else {
            System.out.println(currentFootballGame);
        }
    }

    /**
     * Returns the current football game that is active on this board
     *
     * @return current football game on this board
     */
    public FootballGame getCurrentFootballGame() {
        return currentFootballGame;
    }

    /**
     * Returns list of football games previously handled by this scoreboard
     *
     * @return list of previous football games handled by this scoreboard
     */
    public ArrayList<FootballGame> getPreviousFootballGame() {
        return previousFootballGame;
    }
}
