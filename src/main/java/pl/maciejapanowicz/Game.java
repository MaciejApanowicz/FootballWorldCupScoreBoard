package pl.maciejapanowicz;

/**
 * A game that can be displayed on a Scoreboard.
 */
public interface Game {

    /**
     * Returns the number of participants in this game. Must be an integer greater or equal to 0.
     *
     * @return the number of participants in this game.
     */
    int numberOfParticipants();

    /**
     * Returns true if this game has any participant.
     *
     * @return true if this game contains any participant.
     */
    boolean hasParticipants();

}
