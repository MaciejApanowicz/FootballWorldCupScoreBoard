package pl.maciejapanowicz;

/**
 * Instance of this enumerated class represents a country that is a participant of the Football World Cup 2018.
 */

public enum WorldCup2018Participant implements GameParticipant {
    ARGENTINA("Argentina"),
    AUSTRALIA("Australia"),
    BELGIUM("Belgium"),
    BRAZIL("Brazil"),
    COLOMBIA("Colombia"),
    COSTA_RICA("Costa-Rica"),
    CROATIA("Croatia"),
    DENMARK("Denmark"),
    EGYPT("Egypt"),
    ENGLAND("England"),
    FRANCE("France"),
    GERMANY("Germany"),
    ICELAND("Iceland"),
    IRAN("Iran"),
    JAPAN("Japan"),
    MEXICO("Mexico"),
    MOROCCO("Morocco"),
    NIGERIA("Nigeria"),
    PANAMA("Panama"),
    PERU("Peru"),
    POLAND("Poland"),
    PORTUGAL("Portugal"),
    RUSSIA("Russia"),
    SAUDI_ARABIA("Saudi-Arabia"),
    SENEGAL("Senegal"),
    SERBIA("Serbia"),
    SOUTH_KOREA("South-Korea"),
    SPAIN("Spain"),
    SWEDEN("Sweden"),
    SWITZERLAND("Switzerland"),
    TUNISIA("Tunisia"),
    URUGUAY("Uruguay");

    private final String participantName;

    WorldCup2018Participant(String participantName) {
        this.participantName = participantName;
    }

    @Override
    public String getParticipantName() {
        return participantName;
    }
}
