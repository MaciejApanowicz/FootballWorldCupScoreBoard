package pl.maciejapanowicz;

import java.time.LocalDate;
import java.util.*;

/**
 * There are always two teams in a football game - home team and away team.
 * When a game starts both teams have a score of 0. The score of a team is an integer that can only increase.
 * Football match can be displayed in a Football ScoreBoard.
 *
 * @author Maciej Apanowicz
 */
public class FootballGame implements Game, Comparable<FootballGame> {

    private final int numberOfParticipants;
    private final GameParticipant homeTeam;
    private final GameParticipant awayTeam;
    private Integer homeTeamScore;
    private Integer awayTeamScore;
    private Integer totalScore;
    private LocalDate footballGameDate;
    private int gameId;
    private static Integer nextGameId = 1;

    /**
     * Constructs a new football match, set a default football game date as today.
     * Requires params: homeTeam and awayTeam that must not equal to each other.
     *
     * @param homeTeam game participant playing as a host
     * @param awayTeam game participant playing as a guest
     */
    public FootballGame(GameParticipant homeTeam, GameParticipant awayTeam) {
        if (homeTeam.equals(awayTeam)) {
            throw new IllegalArgumentException("Home team must differ from away team!");
        }
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.homeTeamScore = 0;
        this.awayTeamScore = 0;
        this.numberOfParticipants = 2;
        this.footballGameDate = LocalDate.now();
    }

    /**
     * Constructs a new football match, allows to set a football game date other than today.
     * Requires params: homeTeam and awayTeam that must not equal to each other.
     *
     * @param homeTeam   game participant playing as a host
     * @param awayTeam   game participant playing as a guest
     * @param year       year of football game
     * @param month      month of football game
     * @param dayOfMonth day of football game
     */
    public FootballGame(GameParticipant homeTeam, GameParticipant awayTeam, int year, int month, int dayOfMonth) {
        if (homeTeam.equals(awayTeam)) {
            throw new IllegalArgumentException("Home team must differ from away team!");
        }
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.homeTeamScore = 0;
        this.awayTeamScore = 0;
        this.numberOfParticipants = 2;
        this.footballGameDate = LocalDate.of(year, month, dayOfMonth);
    }

    /**
     * Returns the number of goals scored by home team
     *
     * @return homeTeamScore
     */
    public int getHomeTeamScore() {
        return homeTeamScore;
    }

    /**
     * Returns the number of goals scored by away team
     *
     * @return awayTeamScore
     */
    public int getAwayTeamScore() {
        return awayTeamScore;
    }

    /**
     * returns game participant playing as a host
     *
     * @return hostTeam
     */
    public GameParticipant getHomeTeam() {
        return homeTeam;
    }

    /**
     * Sets home team score. Should be used during a football game to update the home team score
     *
     * @param homeTeamScore number of goals that home team has scored
     */
    public void setHomeTeamScore(Integer homeTeamScore) {
        this.homeTeamScore = homeTeamScore;
    }

    /**
     * Sets away team score. Should be used during a football game to update the away team score
     *
     * @param awayTeamScore number of goals that away team has scored
     */
    public void setAwayTeamScore(Integer awayTeamScore) {
        this.awayTeamScore = awayTeamScore;
    }

    /**
     * returns game participant playing as a guest
     *
     * @return awayTeam
     */
    public GameParticipant getAwayTeam() {
        return awayTeam;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param other object with which to compare
     * @return true if this object is the same as the other argument; false otherwise.
     */

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        FootballGame that = (FootballGame) other;
        return numberOfParticipants == that.numberOfParticipants && Objects.equals(homeTeam, that.homeTeam) && Objects.equals(awayTeam, that.awayTeam) && Objects.equals(getHomeTeamScore(), that.getHomeTeamScore()) && Objects.equals(getAwayTeamScore(), that.getAwayTeamScore()) && Objects.equals(footballGameDate, that.footballGameDate);
    }

    /**
     * Returns a hash code value for this object.
     *
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return Objects.hash(numberOfParticipants, homeTeam, awayTeam, getHomeTeamScore(), getAwayTeamScore(), footballGameDate);
    }

    /**
     * Returns the number of participants of this game.
     *
     * @return the number of participants.
     */
    @Override
    public int numberOfParticipants() {
        return numberOfParticipants;
    }

    /**
     * Returns true if the game has any participants, otherwise returns false.
     *
     * @return true if there is any participant in this game.
     */
    @Override
    public boolean hasParticipants() {
        return numberOfParticipants > 0;
    }

    /**
     * Returns a String that can be used for displaying a football game on the board.
     *
     * @return String representation of the football game.
     */
    @Override
    public String toString() {
        return homeTeam.getParticipantName() + " - " + awayTeam.getParticipantName() + " [" + homeTeamScore + ":" + awayTeamScore + ']';
    }

    /**
     * Calculate total score of both teams when the game is finished
     */
    public void setTotalScore() {
        this.totalScore = homeTeamScore + awayTeamScore;
    }

    /**
     * Returns total number of goals scored by both: home team and away team
     *
     * @return total score of this game
     */
    public Integer getTotalScore() {
        return totalScore;
    }

    /**
     * Returns gameId of this football games
     *
     * @return game id of this game
     */
    public Integer getGameId() {
        return gameId;
    }

    /**
     * Sets gameId of this football game
     */
    public void setGameId() {
        gameId = nextGameId;
        nextGameId++;
    }

    @Override
    public int compareTo(FootballGame other) {
        return Integer.compare(totalScore - other.totalScore, 0);
    }
}
