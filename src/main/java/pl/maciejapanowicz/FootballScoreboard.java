package pl.maciejapanowicz;

import java.util.List;

/**
 * Football scoreboard interface that defines the behaviour of any scoreboard that is intended to handle a football game.
 * To implement this interface implementers must define how to start, update and finish a football game.
 * Concrete class solution must also provide a way to get a summary of the previous football games handled by a board.
 *
 * @author Maciej Apanowicz
 */
public interface FootballScoreboard extends Scoreboard {
    /**
     * Starts a football game between two teams.
     *
     * @param homeTeam a team that plays as a host
     * @param awayTeam a team that plays as a guest
     */
    void startGame(GameParticipant homeTeam, GameParticipant awayTeam);

    /**
     * Starts a football game between two teams, sets the date when the game will take place.
     *
     * @param homeTeam a team that plays as a host
     * @param awayTeam a team that plays as a guest
     * @param year     year when the game will take place
     * @param month    month represented by integer value when the game will take place
     * @param day      day when the game will take place
     */
    void startGame(GameParticipant homeTeam, GameParticipant awayTeam, int year, int month, int day);

    /**
     * Updates current football game on this board. Throws exception when the user tries to update a football game when there is not any game on the board
     *
     * @param homeTeamScore number of goals scored by the home team
     * @param awayTeamScore number of goals scored by the away team
     * @throws Exception thrown when this method is called when no game on the board exists
     */
    void updateFootballGame(Integer homeTeamScore, Integer awayTeamScore) throws Exception;

    /**
     * Finishes a football game
     */
    void finishFootballGame();

    /**
     * Gets a summary of the previous football games handled by a board
     *
     * @return list of football games displayed on this board
     */
    List<FootballGame> getPreviousGamesSummary();
}
