package pl.maciejapanowicz;

/**
 * Basic Scoreboard that is capable of showing a game.
 *
 * @author Maciej Apanowicz
 */
public interface Scoreboard {

    /**
     * Displays the current game on the board
     */
    void displayCurrentGame();
}
