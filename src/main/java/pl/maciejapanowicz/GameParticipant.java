package pl.maciejapanowicz;

/**
 * A game participant that takes part in the game.
 */
public interface GameParticipant {

    /**
     * Returns the game participant name.
     *
     * @return participantName
     */
    String getParticipantName();
}
