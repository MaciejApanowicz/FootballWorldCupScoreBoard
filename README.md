# Football World Cup Score Board

***
Football World Cup Score Board library delivers simple tools that can be used to show a football match with the
corresponding score. While the main implementation of this project delivers solution for handling football world cup
matches, this library also provide high flexibility by offering the interface layer. At the top of which is the
Scoreboard interface capable of handling any game type, so the use of this library can be consistent with other game
types and leaves open doors for further development.

Implementation that is offered in concrete WorldCup2018FootballScoreboard class supports following operations:
start a football game, update started game with the score received, finish the current game and make this scoreboard
available for another game. When a game is over the data is collected then it is possible to get a summary of previous
games handled by the board. Those games are sorted by total score and if total score is equal by the most recently
added.

This is World Cup 2018 not World Cup 2022 scoreboard, because as of today (17.05.2022) not all teams have been qualified
yet, with some open spots left - hence I chose to create enum class consists all 2018 (last World Cup)
participants. Simply add enum WorldCup2022Participant when all teams are qualified, and you will have a model ready to
use for World Cup 2022.
***

## Installation / use / adding to project

This library can be added to your java project just like any library, by adding it to the project dependencies.  
Recommended way is to simply build this library by executing gradle build task (Gradle-> Tasks-> build-> build) and then
publish it to your local maven repository by executing gradle publishToMavenLocal task (Gradle-> Tasks-> publishing->
publishToMavenLocal). When that is done, just put dependency to the build.gradle file in your consumer project. Below is
an example how to add this library to a Gradle project:

repositories { mavenLocal() }     
dependencies{ implementation group: 'pl.maciejapanowicz', name: 'FootballWorldCupScoreBoard', version: '1.0.0' }
***

## Executing code example

- WorldCup2018FootballScoreboard yourAwesomeBoard = new WorldCup2018FootballScoreboard();  
  // create your awesome scoreboard


- yourAwesomeBoard.startGame(WorldCup2018Participant.POLAND, WorldCup2018Participant.SENEGAL);  
  // start a game that will be displayed on your awesome board


- yourAwesomeBoard.updateFootballGame(1,2);  
  // after receiving the score, the game on the board will be updated


- yourAwesomeBoard.finishFootballGame();  
  // finish the game, it will be added to the previous games and the board ready for another game


- ArrayList<FootballGame> previousGames = yourAwesomeBoard.getPreviousFootballGame();  
  // get a summary of previous games sorted by total score, if the same then by the most recently added order


- previousGames.forEach(System.out::println);  
  // have got sorted games at this point, do whatever you like with them, print them to the console as example

Execution of above code example will lead you to the output on your console:  
Poland - Senegal [1:2]     
// oh, no... I still remember that disaster... how about you? :-(
***

## Author

- Maciej Apanowicz   
  e-mail: maciej.apanowicz@gmail.com   
  tel: + 48 696-002-782

***

## Version History

Version history is precisely described in CHANGELOG.md file  
Please review that file for the information about version history
***

## Final remarks

This very first stable version [1.0.0] of this project is a response to the code exercise recruitment task delegated by
Sportradar company. I put a lot of effort in: designing this library, writing unit tests, creating a test pipeline on
GitLab, writing full javadoc documentation, implementing expected project functionalities, taking care about solid
rules, clean code standards. However, I suppose it could have been developed better, therefore your extensive and
accurate feedback would be very much appreciated. Thank you for your time spent on reviewing this project.
